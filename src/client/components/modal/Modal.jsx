import React, {Fragment} from 'react';
import injectSheet from 'react-jss';

const styles = () => ({
 container:{
  display: 'block', 
  position: 'fixed', 
  zIndex: 1, 
  left:0,
  top: 0,
  width: '100%', 
  height: '100%', 
  backgroundColor: 'rgba(0,0,0,0.4)',
 },
 containerNone: {
  display: 'none',
 },
 modal:{
  right: 0,
  top: 0,
  backgroundColor: 'white',
  position: 'absolute',
  width: '45%',
  minHeight: '100%',
  display: 'flex',
  justifyContent: 'space-between',
  flexDirection: 'column',
  alignItems: 'center',
 },
})

class Modal extends React.Component {
  
  render() {
    const {classes, hidden} = this.props;
    return (
      <Fragment >
        <div className={hidden ? classes.containerNone : classes.container}>
          <div className={classes.modal}>
            {this.props.children}
          </div>
          
        </div>
      </Fragment>
    );
  }
}


export default injectSheet(styles)(Modal);