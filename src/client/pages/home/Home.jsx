import React, {Fragment} from 'react';
import injectSheet from 'react-jss';
import Catalog from './components/Catalog';
import Info from './components/Info';
import refuge from '../../data/refuge.json';

const styles = {

}

class Home extends React.Component {

    render() {
        const {classes} = this.props;
        return (
            <Fragment >
                <Catalog data={refuge.content}/>
                <Info />
            </Fragment>
        );
    }
}

export default injectSheet(styles)(Home);
