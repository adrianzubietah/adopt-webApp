import React, {Fragment} from 'react';
import injectSheet from 'react-jss';

const styles = theme => ({
  card: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    background: 'white',
    borderRadius: '5%',
    width: '20%',
    margin: '1% 1% 3% 1%',
    justifyContent: 'space-between',
    paddingBottom: '1%',
  },
  cardHover: {
    boxShadow: [0, 0, 25, 'rgba(0, 0, 0, 0.5)']
  },
  image: {
    borderRadius: '5% 5% 0% 0%',
    width:  '100%',
    height: '60%',
  },
  name: {
    fontSize: 'xx-large',
    padding:'4% 0% 4% 0%',
    fontFamily: 'Segoe UI'
  },
  [`@media (min-width: ${theme.spacing.tablet}px)`]: {},
  [`@media (min-width: ${theme.spacing.desktop}px)`]: {}
})

class Card extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      hover: false
    }
  }

  toggleHover = (event) => {
    this.setState({hover: !this.state.hover})
  }
  
  render() {
    const {classes, data} = this.props;
    return (
      <Fragment >
        <div  className={this.state.hover ? classes.card + " " + classes.cardHover: classes.card} 
        key={data.id} 
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}>
          <img className={classes.image} alt={data.name} title={'photo'} src={data.urlPhoto}/>
          <div className={classes.name}>{data.name}</div>
          <div>{data.description}</div>
          <div>{data.city.name}</div>
        </div>
      </Fragment>
    );
  }
}


export default injectSheet(styles)(Card);