import React, {Fragment} from 'react';
import injectSheet from 'react-jss';
import data from '../../../data/oneAnimal.json';
import { Drawer } from 'antd';

const styles = theme => ({
  container: {
    padding: '5%',
  },
  containerImage:{
    width: '90%',
    height: '50%',
    padding: '2%'
  },
  image: {
    height: '100%',
    width:  '100%',
  },
  [`@media (min-width: ${theme.spacing.tablet}px)`]: {},
  [`@media (min-width: ${theme.spacing.desktop}px)`]: {}
})

class Info extends React.Component {

  state = { visible: true };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const {classes } = this.props;
    return (
      <Fragment >
        <Drawer
          title="Basic Drawer"
          closable={false}
          width={'40%'}
          onClose={this.onClose}
          visible={this.state.visible}
        >
          <div className={classes.container}>
              <div className={classes.containerImage}>
                  <img className={classes.image} alt={data.name} title={'photo'} src={data.urlPhoto}/>
              </div>
              <span>
              {data.history}
              </span>
          </div>
        </Drawer>
      </Fragment>
    );
  }
}


export default injectSheet(styles)(Info);