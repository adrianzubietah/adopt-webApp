import React, {Fragment} from 'react';
import injectSheet from 'react-jss';
import {connect} from 'react-redux';
import Card from './Card';

const styles = theme => ({
  container:{
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    background: '#ebebeb',
  },
  [`@media (min-width: ${theme.spacing.tablet}px)`]: {},
  [`@media (min-width: ${theme.spacing.desktop}px)`]: {}
})

class Catalog extends React.Component {
  
  render() {
    const {classes, data} = this.props;
    return (
      <Fragment >
        <div  className={classes.container}>
        {
          data.map((item, i) => {
            return (
              <Card key={item.id} data={item}/>
            )
          })
        }
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    login: state
  }
}


const ConnectLogin = connect(
  mapStateToProps
)(Catalog);

export default injectSheet(styles)(ConnectLogin);